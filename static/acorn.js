function code() {
    let opentoggle = document.querySelector(".nav-toggle")
    let navmenu = document.querySelector(".nav-menu")
    let navcontent = document.querySelector(".nav-content")
    let open = false
    opentoggle.addEventListener("click", function() {
        if (open == false) {
            navmenu.style.height = navcontent.offsetHeight + 30
            open = true
        } else {
            navmenu.style.height = 0
            open = false
        }
    })
}
if (!!(window.addEventListener)) window.addEventListener("DOMContentLoaded", code); else window.attachEvent("onload", code);