const express = require("express"),
    app = express(),
    Keys = require("./keys"),
    config = require("./config"),
    bodyParser = require("body-parser"),
    fs = require("fs-extra"),
    exhbs = require('express-handlebars');

let hbs = exhbs.create({
    extname: ".hbs",
    helpers: {
        eq: function (v1, v2) {
            return v1 == v2;
        },
        ne: function (v1, v2) {
            return v1 != v2;
        },
        lt: function (v1, v2) {
            return v1 < v2;
        },
        gt: function (v1, v2) {
            return v1 > v2;
        },
        lte: function (v1, v2) {
            return v1 <= v2;
        },
        gte: function (v1, v2) {
            return v1 >= v2;
        },
        and: function (v1, v2) {
            return v1 && v2;
        },
        or: function (v1, v2) {
            return v1 || v2;
        }
    }
})

app.set("views", "./views")
app.engine("hbs", hbs.engine)
app.set("view engine", "hbs")
app.use(bodyParser())
app.use("/static", express.static("./static"))

app.get("/", (req, res) => {
    res.render("index", { layout: "page", path: "index" })
})

app.get("/about", (req, res) => {
    res.render("about", { layout: "page", path: "about" })
})

app.get("/projects", (req, res) => {
    res.render("projects", { layout: "page", path: "projects" })
}) 

app.listen(9082, "127.0.0.1")